import React, { Component } from 'react';

import './PostCard.scss'
export default class PostCard extends Component {
  render() {
    return (
        <div className={`card col-md-${this.props.mdRef} col-lg-${this.props.lgRef} col-sm-${this.props.smRef}  mr-md-4  mt-md-4 postCard`}  >
            <img className="card-img-top " src={this.props.json.postImage} height="250" alt="" />
            <div className="card-body">
                <h5 className="card-title">{this.props.json.postTitle}</h5>
                <p className="card-text">{this.props.json.postDescription}</p>
          </div>
        </div>
    );
  }
}

