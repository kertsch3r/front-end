import React, { Component } from 'react';

import './ScrollToTopButton.scss'
import $ from 'jquery';

export default class ScrollToTopButton extends Component {

  scrollTop() {
       $(`#${this.props.divReferenceId}`).animate({ scrollTop: 0 }, 1000);
   }
  render() {
    return (
        <button className="btn backToTopButton" onClick={() => this.scrollTop()}>voltar ao topo</button>
    );
  }
}

