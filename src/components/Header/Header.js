import React, { Component } from 'react';


import { faBars,faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {Link} from 'react-router-dom'
import './Header.scss'
export default class Header extends Component {

    constructor(props){
        super(props)
        this.state ={
            showMenu:false
        }

        this.showMenu = this.showMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
    }   

    showMenu(event) {
        event.preventDefault();
        
        this.setState({ showMenu: true }, () => {
          document.addEventListener('click', this.closeMenu);
        });
      }


      closeMenu() {
        this.setState({ showMenu: false }, () => {
          document.removeEventListener('click', this.closeMenu);
        });
      }

    render() {
        return (
            <div className="mainHeader container-fluid h-40">    


                <div className="row">

                <div className="col-3 d-flex">
                    <button className="btn menuButton" onClick={this.showMenu}><FontAwesomeIcon icon={faBars} size="sm"  /></button>
                    {
                    this.state.showMenu
                        ? (
                        <div className="menu d-flex flex-column card">
                            <ul className="list-group">
                            
                                <li className="list-group-item"><Link to="/">Home</Link></li>
                                <li className="list-group-item"><Link to="/Heroes">Heróis</Link></li>
                            </ul>
                        </div>
                        )
                        : (
                        null
                        )
                    }
                </div>
                <div className="col-6  d-flex justify-content-center align-items-center" >
                    <span> <strong>Bem vindo!</strong> </span>
                </div>
                <div className="col-3  d-flex  justify-content-end">
                    <button className="btn" ><FontAwesomeIcon icon={faSignOutAlt} size="sm" /></button>
                </div>   
                </div>        
                       
        </div>
        );
    }
}

