import React, { Component } from 'react';
import axios from 'axios';
import md5 from 'md5'
import './HeroForm.scss';

import ScrollToTopButton from '../../components/ScrollToTopButton/ScrollToTopButton'
import ClipLoader from 'react-spinners/ClipLoader';
import { css } from '@emotion/core';


const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;
 
const PUBLIC_KEY = "bbf0986e5a9fd8f87c6aaa6b850f2593"
const PRIVATE_KEY = "b18e1e7dba2d621eff01596daeb2878a2770e28c"
const API_URL = "http://gateway.marvel.com/v1/public"


export default class HeroForm extends Component {



  constructor(props){
    super(props)

      this.state = {
        loading: false,
        content: {
          thumbnail:{
            
          },
        },
        comics:[]
      }
  }


  


  fetchData(){
    this.setState({loading:true})
    let heroId = this.props.match.params.id
    let timestamp = Math.floor(Date.now() / 1000)

    let string = `${timestamp}${PRIVATE_KEY}${PUBLIC_KEY}`
    let hash = md5(string)

    let params = {
      apikey:PUBLIC_KEY,
      hash:hash,
      ts:timestamp,
      limit:40
    }


    axios.get(`${API_URL}/characters/${heroId}`,{
      params:params
    })
    .then(res => {
      this.setState({content:res.data.data.results[0]})
      axios.get(`${API_URL}/characters/${heroId}/comics`,{
        params:params
      })
      .then(res => {
        this.setState({comics:res.data.data.results})
        this.setState({loading:false})      
      }).catch(err => console.log(JSON.stringify(err)))
      
    }).catch(err => console.log(JSON.stringify(err)))
  }

  componentWillMount() {
    this.fetchData()
  }




  render() {
    return (
      this.state.loading === true ? <div className="container-fluid d-flex justify-content-center align-items-center" > <ClipLoader css={override} sizeUnit={"px"} size={200}color={'#383838'} loading={this.state.loading}/> </div> 
      :
      <div className="container flexContainer d-flex justify-content-center">
                    
          <div className="row formWrapper tet" id="scrollThis">
              <div className="col-12 d-flex flex-column">
                  <img src={`${this.state.content.thumbnail.path}/standard_xlarge.${this.state.content.thumbnail.extension}`} alt="" className="profileImage" />
                  <h3 className="heroNameForm">{this.state.content.name}</h3>
              </div>
              <div className="col-12 heroDescription" >
                    {this.state.content.description !== "" ? this.state.content.description : "Esse herói não possui uma descrição." }
              </div>
              <div className="col-12 comicWrapper">
                    <h2>Algumas histórias com esse herói</h2>
                  
                    <div className="row">
                          { 
                            this.state.comics.map((comic,index) => {
                              return(
                                <div className={`col-xs-12 col-lg-${this.state.comics.length < 4 ?  `${12 / this.state.comics.length}` : '3'} col-md-${this.state.comics.length < 4 ?  `${12 / this.state.comics.length}` : '3'} mt-4 d-flex justify-content-center`} key={index} >
                                     <img src={`${comic.thumbnail.path}/portrait_xlarge.${this.state.content.thumbnail.extension}`} alt=""/>   

                                </div>  
                              )
                            })
                          }
                    </div>

                   
                    
              </div>
              
              <div className="col-12 " style={{padding:30,textAlign:'center'}}>
                    {
                      this.state.comics.length > 0 ? <ScrollToTopButton divReferenceId="scrollThis" /> : <h2>Não encontramos histórias com esse personagem</h2>
                    }
                    
              </div>
          </div>

      </div>
    );
  }
}

