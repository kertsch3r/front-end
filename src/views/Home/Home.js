import React, { Component } from 'react';

import './Home.scss'

import PostCard from '../../components/PostCard/PostCard'
import PostsArray from './Posts.json'
import ScrollToTopButton from '../../components/ScrollToTopButton/ScrollToTopButton'

export default class Home extends Component {
  render() {
    return (       
      <div className="container-fluid tet" id="scrollThis">
        <div className="row justify-content-center" >
          {
            PostsArray.map(x => {
              return(
                <PostCard lgRef="3" mdRef="4" smRef="3" json={x} key={x.id}/>
              )
            })
          }

          <div className="col-12" style={{padding:30}}>
              <ScrollToTopButton divReferenceId="scrollThis"/>
          </div>    
        </div>

    </div>
    );
  }
}

