import React, { Component } from 'react';
import './HeroList.scss'

import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {Link} from 'react-router-dom'
import axios from 'axios';
import md5 from 'md5'
import ClipLoader from 'react-spinners/ClipLoader';
import { css } from '@emotion/core';


const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;
 
const PUBLIC_KEY = "bbf0986e5a9fd8f87c6aaa6b850f2593"
const PRIVATE_KEY = "b18e1e7dba2d621eff01596daeb2878a2770e28c"
const API_URL = "http://gateway.marvel.com/v1/public"


export default class HeroList extends Component {

  constructor(props){
      super(props)

      this.state = {
         loading: false,
         searchField:null,
         currentPage:1,
         itemsPerPage:8,
         content: {
            results:[],
            count:0,
            limit:0,
            offset:0,
            total:0
         }
      }
  }

  fetchData(startsWith,offset,returnToStart){

    this.setState({loading:true})
    let timestamp = Math.floor(Date.now() / 1000)

    let string = `${timestamp}${PRIVATE_KEY}${PUBLIC_KEY}`
    let hash = md5(string)

    let params = {
      apikey:PUBLIC_KEY,
      hash:hash,
      ts:timestamp,
      limit:8
    }

    if(startsWith != null){
      params.nameStartsWith = startsWith
    }

    if(offset != null){
      params.offset = offset
    }

    axios.get(`${API_URL}/characters`,{
      params:params
    })
    .then(res => {
      this.setState({content:res.data.data})
      if(returnToStart === true){
        this.setState({currentPage:1})
      }
      this.setState({loading:false})
    }).catch(err => console.log(JSON.stringify(err)))
  }

  componentWillMount() {
    this.fetchData()
  }

  searchName = () =>{
    console.log("CALLED SEARCH")
    this.fetchData(this.state.searchField,null,true)
  }


  nextPage(){
      let totalPages = Math.ceil(this.state.content.total / 8)

      if(this.state.currentPage < totalPages){

        
        let offset = this.state.currentPage  * this.state.itemsPerPage

        let paged = this.state.currentPage 
        paged += 1
        
        this.setState({currentPage:paged})
       
        if(offset > this.state.content.total){
          offset = this.state.itemsPerPage
        }
        this.fetchData(this.state.searchField,offset)
      }else alert("Não há itens seguintes")

    
  }

  
  previousPage(){

    if(this.state.currentPage > 1 ){
        
    let paged = this.state.currentPage 
    paged -= 1

    let offset
    if(paged === 1){
      offset = 0
    }else  offset = (paged - 1)  * this.state.itemsPerPage
  
    this.setState({currentPage:paged})
    this.fetchData(this.state.searchField,offset)

    }else alert("Não há itens anteriores")

  }

  render() {

    return (
      this.state.loading === true ? <div className="container-fluid d-flex justify-content-center align-items-center" > <ClipLoader css={override} sizeUnit={"px"} size={200}color={'#383838'} loading={this.state.loading}/> </div> 
      :
      <div className="container-fluid flexContainer noPadding">
         <div className="card flex-grow-1">
            <div className="card-header d-flex justify-content-between align-items-center ">
                <strong>Lista de Heróis</strong>
                <div className="input-group searchInput">
                <input type="text" className="form-control" placeholder="Pesquise aqui"  onChange={(event) => {this.setState({searchField:event.target.value})}} />
                  <div className="input-group-prepend">
                    <button className="input-group-text" id="basic-addon3" onClick={() =>{ this.searchName()}} ><FontAwesomeIcon icon={faSearch} size="sm"  /></button>
                  </div>
                </div>
            </div> 
            <div className="card-body noPadding test" >
              <ul className="list-group noPadding ">
                 {this.state.content.results.map((hero,index) => {
                    return(
                      <li className="list-group-item listItem d-flex align-items-center" key={index}>
                        <span className="pull-left imageHolder">
                          <img src={`${hero.thumbnail.path}/standard_xlarge.${hero.thumbnail.extension}`}  alt=""  className="img-reponsive img-rounded heroImage" />
                        </span>
                        <span className="heroName">
                          <Link to={`/Heroes/View/${hero.id}`}>{hero.name}</Link>         
                        </span>                 
                      </li>
                    )
                 })}
              </ul>
              
            </div> 
            <div className="card-footer d-flex justify-content-end">
              <ul className="pagination" >
                <li className="page-item d-flex align-items-center" >Página {this.state.currentPage} de {Math.ceil(this.state.content.total / 8)}</li>
                <li className="page-link" style={{marginLeft:20}} onClick={() =>{ this.previousPage()}}>Anterior</li>
                <li className="page-link" onClick={() =>{ this.nextPage()}}>Próximo</li>      
              </ul>
            </div>
         </div>
      </div>
    );
  }
}

