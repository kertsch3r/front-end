import React, { Component } from 'react';
import './styles/App.scss';


import {Route,Switch } from 'react-router-dom'


import Home from './views/Home/Home'
import HeroList from './views/HeroList/HeroList'
import HeroForm from './views/HeroForm/HeroForm'


import Header from './components/Header/Header'

class App extends Component {
  render() {
    return (
      <div className="container-fluid mainWrapper" >
          <div className="d-flex flex-column flex-grow-1 h-100">
            <Header/>
             <div className="contentContainer d-flex flex-grow-1">
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/Heroes" exact component={HeroList} />
                    <Route path="/Heroes/:viewMode/:id" exact component={HeroForm} />
                </Switch>
             </div>
          </div>
      </div>


  

       
    );
  }
}

export default App;
