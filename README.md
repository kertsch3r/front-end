## Juno frontend challenge

Este repositório contém o código frontend do teste disponível no link https://github.com/boletofacil/tests/tree/master/front-end-test 

---

## Instalação

Assim que você clonar o repositório execute os comandos

1. No terminal rode o comando npm install e aguarde a exceução do comando
2. Após a instalação, rode o comando npm start para subir a aplicação
3. Acesse http://localhost:3000 em seu navegador

---

## Feedback

Sua opinião é muito importante para mim, qualquer dúvida ou feedback é só me mandar um email em filipekertcher97@gmail.com :) 
